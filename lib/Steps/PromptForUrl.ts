import { Answers, prompt } from 'inquirer';
import * as _ from 'lodash';

import { BaseStep } from './BaseStep';

export class PromptForUrl extends BaseStep {
  public async emit(answers: Answers): Promise<Answers> {
    this.debug(answers);

    let { url }: any = await prompt([
      {
        message: 'GlitchTip instance URL:',
        name: 'url',
        type: 'input',
        // eslint-disable-next-line @typescript-eslint/unbound-method
        validate: this._validateUrl,
      },
    ]);
    url = this._sanitiseUrl(url);

    return {
      config: _.merge(_.get(answers, 'config'), {
        url
      }),
    };
  }

  private _validateUrl(input: string): boolean | string {
    if (input.length === 0) {
      return 'Can\'t be empty';
    }
    if (!input.startsWith('http')) {
      return 'Must be the full URL to the instance e.g. https://...'
    }
    return true;
  }

  private _sanitiseUrl(url: string): string {
    let baseUrl = url;
    baseUrl += baseUrl.endsWith('/') ? '' : '/';
    baseUrl = baseUrl.replace(/:\/(?!\/)/g, '://');
    return baseUrl
  }
}
