import { Answers } from 'inquirer';

import { dim, green } from '../Helper/Logging';
import { BaseStep } from './BaseStep';

export class Welcome extends BaseStep {
  private static _didShow: boolean = false;

  public async emit(_answers: Answers): Promise<Answers> {
    if (Welcome._didShow) {
      return {};
    }
    if (this._argv.uninstall === false) {
      green('GlitchTip Wizard will help you to configure your project');
      dim('Thank you for using GlitchTip :)');
    }
    Welcome._didShow = true;
    return {};
  }
}
